<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Test Suite Web</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>2006283e-d422-4d9b-bb98-f86f3762cf20</testSuiteGuid>
   <testCaseLink>
      <guid>e2cd3262-dfbf-47bc-a674-673d7fb6e427</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Web Test Case</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>625f54e7-214f-40d4-84c1-b49547473d10</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Test Data All</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>625f54e7-214f-40d4-84c1-b49547473d10</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>email</value>
         <variableId>1410ae23-2e5a-4491-ae03-dec131993999</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>625f54e7-214f-40d4-84c1-b49547473d10</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>22d4d2de-97bb-4480-bd63-017dfc219101</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
