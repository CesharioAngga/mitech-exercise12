<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Test Suite Mobile</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>fa177e25-7f47-432d-8005-4b58d88c1d30</testSuiteGuid>
   <testCaseLink>
      <guid>67b4021c-b0ad-4081-9806-f91a5affb0d4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mobile Test Case</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>8d47ed47-1fbc-45e5-ac4b-f5eea2588ba7</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Test Data All</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>8d47ed47-1fbc-45e5-ac4b-f5eea2588ba7</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>email</value>
         <variableId>12fb2e46-40de-405d-a6dc-7c1b80dfc0ef</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>8d47ed47-1fbc-45e5-ac4b-f5eea2588ba7</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>42dd5882-5cda-404d-98fc-9c45e0d00c87</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
