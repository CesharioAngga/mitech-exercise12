import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://bro.do/')

WebUI.click(findTestObject('Object Repository/Web Object/Page_BRODO Store  Brodo/profil'))

WebUI.setText(findTestObject('Object Repository/Web Object/Page_Account  Brodo/input_Email'), email)

WebUI.setText(findTestObject('Object Repository/Web Object/Page_Account  Brodo/input_Password'), password)

WebUI.click(findTestObject('Object Repository/Web Object/Page_Account  Brodo/Button login'))

WebUI.click(findTestObject('Object Repository/Web Object/Page_Account  Brodo/Tab_NON-FOOTWEAR'))

WebUI.click(findTestObject('Web Object/Page_All Essentials  Brodo/product'))

WebUI.click(findTestObject('Object Repository/Web Object/Page_Varsy Hat Brown/button_Add to cart'))

WebUI.setText(findTestObject('Object Repository/Web Object/Page_Varsy Hat Brown/add note product'), 'hitam')

WebUI.click(findTestObject('Object Repository/Web Object/Page_Varsy Hat Brown/button_Check out'))

WebUI.click(findTestObject('Object Repository/Web Object/Page_Checkout - Brodo/button chart'))

WebUI.verifyElementPresent(findTestObject('Web Object/Page_Account  Brodo/Verify Element chart'), 0)

WebUI.closeBrowser()

