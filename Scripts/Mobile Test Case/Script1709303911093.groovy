import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('C:\\Users\\Angga\\Downloads\\Brodo_4.1_apkcombo.com.apk', true)

Mobile.tap(findTestObject('Object Repository/Mobile Object/Menu 1'), 0)

Mobile.tap(findTestObject('Object Repository/Mobile Object/login'), 0)

Mobile.setText(findTestObject('Object Repository/Mobile Object/email'), email, 0)

Mobile.setText(findTestObject('Object Repository/Mobile Object/password'), password, 0)

Mobile.tap(findTestObject('Object Repository/Mobile Object/login 2'), 0)

Mobile.tap(findTestObject('Mobile Object/button after login'), 0)

Mobile.tap(findTestObject('Object Repository/Mobile Object/collection'), 0)

Mobile.tap(findTestObject('Object Repository/Mobile Object/shoes collection'), 0)

Mobile.tap(findTestObject('Object Repository/Mobile Object/sneaker shoes'), 0)

Mobile.tap(findTestObject('Object Repository/Mobile Object/choose shoe'), 0)

Mobile.tap(findTestObject('Object Repository/Mobile Object/Button - Add to Bag'), 0)

Mobile.tap(findTestObject('Object Repository/Mobile Object/menu 3'), 0)

Mobile.tap(findTestObject('Object Repository/Mobile Object/menu chart list'), 0)

Mobile.waitForElementPresent(findTestObject('Mobile Object/verify element check out'), 0)

Mobile.closeApplication()

