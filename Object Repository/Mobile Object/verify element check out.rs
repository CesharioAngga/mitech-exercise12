<?xml version="1.0" encoding="UTF-8"?>
<MobileElementEntity>
   <description></description>
   <name>verify element check out</name>
   <tag></tag>
   <elementGuidId>bf603e4a-5822-482c-91ce-2db8cf88d7c3</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <locator>//*[@class = 'android.widget.Button' and (@text = 'CHECKOUT' or . = 'CHECKOUT') and @resource-id = 'co.tapcart.app.id_gCATITBKZP.cart:id/checkoutButton']</locator>
   <locatorStrategy>XPATH</locatorStrategy>
</MobileElementEntity>
