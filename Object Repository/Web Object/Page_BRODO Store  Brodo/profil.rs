<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>profil</name>
   <tag></tag>
   <elementGuidId>64d1769f-400b-4ec4-a124-5e554b64cb1f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a.site-nav__link.site-nav__link--icon.small--hide</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='shopify-section-header']/div[2]/div/header/div/div/div[3]/div/div/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>5696d38a-f4ea-443e-96e2-bcb9446b35ac</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>site-nav__link site-nav__link--icon small--hide</value>
      <webElementGuid>f019d0ea-9c53-43ec-9f00-23a32e27f14b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/account</value>
      <webElementGuid>b2029585-7469-4e4b-b897-09b04477e134</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        
        Log in

      </value>
      <webElementGuid>cb02cc24-9565-44fd-ab56-34affd37f340</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;shopify-section-header&quot;)/div[2]/div[@class=&quot;header-wrapper header-wrapper--overlay is-light&quot;]/header[@class=&quot;site-header&quot;]/div[@class=&quot;page-width&quot;]/div[@class=&quot;header-layout header-layout--right header-layout--mobile-logo-only&quot;]/div[@class=&quot;header-item header-item--icons small--hide&quot;]/div[@class=&quot;site-nav site-nav--icons&quot;]/div[@class=&quot;site-nav__icons&quot;]/a[@class=&quot;site-nav__link site-nav__link--icon small--hide&quot;]</value>
      <webElementGuid>afc6d123-b7d4-41bb-9cfe-5b1a0e69509f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='shopify-section-header']/div[2]/div/header/div/div/div[3]/div/div/a</value>
      <webElementGuid>8b736719-f793-49ed-862f-b930f3ee8295</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='BRO LABS'])[2]/following::a[1]</value>
      <webElementGuid>c89c88b2-ac09-42b6-86ee-433701ebd746</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Return Policy'])[2]/following::a[2]</value>
      <webElementGuid>050bd46f-f971-4414-8d6f-870524ac4f99</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Search'])[2]/preceding::a[1]</value>
      <webElementGuid>745d2c3f-ad54-4436-a0a8-25d14471ffcb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '/account')])[2]</value>
      <webElementGuid>788c4235-6eb4-46f5-bef4-74f6c442f03c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/a</value>
      <webElementGuid>126b5696-2658-45d3-9301-a6653d677376</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/account' and (text() = '
        
        Log in

      ' or . = '
        
        Log in

      ')]</value>
      <webElementGuid>ee4ec00a-5f60-4714-8b01-a91101446f67</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
