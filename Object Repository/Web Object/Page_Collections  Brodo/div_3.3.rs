<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_3.3</name>
   <tag></tag>
   <elementGuidId>e61b249a-5ccc-4391-a009-4118d481e1a7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.skrim__title</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='shopify-section-template--16204430246079__main']/div/div/div/a/div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>0e004194-f1fc-41cb-9223-4e11a6eae908</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>skrim__title</value>
      <webElementGuid>fdb65f32-a19d-4560-a7b8-e5c8cc78947e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
      
        3.3
      
    </value>
      <webElementGuid>c85ed129-f717-4fd3-ac57-b46121941f55</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;shopify-section-template--16204430246079__main&quot;)/div[@class=&quot;page-width page-content aos-init aos-animate&quot;]/div[@class=&quot;skrim-grid appear-delay-1&quot;]/div[@class=&quot;skrim__item&quot;]/a[@class=&quot;skrim__link loaded&quot;]/div[@class=&quot;skrim__title&quot;]</value>
      <webElementGuid>dc19a995-c1d7-458b-97b5-c637bbb2f5d1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='shopify-section-template--16204430246079__main']/div/div/div/a/div[2]</value>
      <webElementGuid>abcf2aad-6578-4c06-8a26-e172f26cf0b8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Catalog'])[1]/following::div[4]</value>
      <webElementGuid>368cad1b-295f-4c83-8896-2a0944acfe48</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Close (esc)'])[1]/following::div[6]</value>
      <webElementGuid>b87bfc54-7b88-4284-86e1-0180b6e3170f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ace Mezzala'])[1]/preceding::div[3]</value>
      <webElementGuid>5ae539e7-d24b-4de4-a256-d0a278d07e5b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ace Nova'])[1]/preceding::div[7]</value>
      <webElementGuid>56216c23-259c-4d84-8356-c5a00855c8c6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a/div[2]</value>
      <webElementGuid>cd5b8a54-60d8-4340-8c38-5579e37f0683</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
      
        3.3
      
    ' or . = '
      
        3.3
      
    ')]</value>
      <webElementGuid>ef46a37e-21de-4ed5-8948-47c630bccb97</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
