<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Sold Out_grid__image-ratio grid__image-_b5a381</name>
   <tag></tag>
   <elementGuidId>86dc7db4-c412-414f-985c-19f2400f67c4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.grid__image-ratio.grid__image-ratio--natural.lazyloaded</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='ProductGridSlider-4437327741033']/div/div/div[2]/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>285ef028-31ab-497a-bedd-96903d3b1240</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>grid__image-ratio grid__image-ratio--natural lazyloaded</value>
      <webElementGuid>c6349995-c7e5-4fd8-a2d0-94e46faa8ac6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-bgset</name>
      <type>Main</type>
      <value>
  //bro.do/cdn/shop/products/1_86a57d23-66c8-47a1-915d-b72919f5ad04_180x.jpg?v=1662435622 180w 180h,
  //bro.do/cdn/shop/products/1_86a57d23-66c8-47a1-915d-b72919f5ad04_360x.jpg?v=1662435622 360w 360h,
  //bro.do/cdn/shop/products/1_86a57d23-66c8-47a1-915d-b72919f5ad04_540x.jpg?v=1662435622 540w 540h,
  //bro.do/cdn/shop/products/1_86a57d23-66c8-47a1-915d-b72919f5ad04_750x.jpg?v=1662435622 750w 750h,
  //bro.do/cdn/shop/products/1_86a57d23-66c8-47a1-915d-b72919f5ad04_900x.jpg?v=1662435622 900w 900h,
  //bro.do/cdn/shop/products/1_86a57d23-66c8-47a1-915d-b72919f5ad04_1080x.jpg?v=1662435622 1080w 1080h,
  
  
  
</value>
      <webElementGuid>2a7caaba-d4b1-4c32-b39a-63830bc8d66e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ProductGridSlider-4437327741033&quot;)/div[@class=&quot;flickity-viewport&quot;]/div[@class=&quot;flickity-slider&quot;]/div[@class=&quot;product-slide is-selected&quot;]/div[@class=&quot;image-wrap loaded&quot;]/div[@class=&quot;grid__image-ratio grid__image-ratio--natural lazyloaded&quot;]</value>
      <webElementGuid>f36af53a-74b4-4169-b39e-88d79c31e454</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='ProductGridSlider-4437327741033']/div/div/div[2]/div/div</value>
      <webElementGuid>3d449bf5-89a2-4712-94b0-21443eebb3ea</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sold Out'])[1]/following::div[9]</value>
      <webElementGuid>e9e9ce4a-6d4d-4b50-b9c5-4bd69fdd4a21</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sort'])[1]/following::div[17]</value>
      <webElementGuid>1b9266f2-4c19-4047-8318-ce533a712bc4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Vulcan Hi Olive Black BS'])[1]/preceding::div[16]</value>
      <webElementGuid>23f54842-0aec-42eb-ac6c-bb96317a7afc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rp 255.000,00'])[1]/preceding::div[17]</value>
      <webElementGuid>56c96c3a-78f8-4378-b31e-dfb424cf83fd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a/div/div/div/div[2]/div/div</value>
      <webElementGuid>2a7a093c-e5b4-4865-bd7e-d695544c094a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
