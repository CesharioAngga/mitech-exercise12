<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button chart</name>
   <tag></tag>
   <elementGuidId>f763c524-1de8-44a2-bd96-f4d222caffd2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>svg.a8x1wuo._1fragemk5._1fragemm9._1fragemhs._1fragemhn</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Checkout'])[1]/following::*[name()='svg'][1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>svg</value>
      <webElementGuid>02818590-cde6-498b-94be-bb2f4955ca00</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xmlns</name>
      <type>Main</type>
      <value>http://www.w3.org/2000/svg</value>
      <webElementGuid>3afc7fba-6e8f-4220-abdc-d73e1b32bb3c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>viewBox</name>
      <type>Main</type>
      <value>0 0 14 14</value>
      <webElementGuid>500aaf87-f970-424c-bf9f-c2854b9c4aa5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>focusable</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>18bcfbe1-67b6-4cac-a8c2-52068060a373</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-hidden</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>7169df9e-4a55-417f-bc93-c8f078b853d6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>a8x1wuo _1fragemk5 _1fragemm9 _1fragemhs _1fragemhn</value>
      <webElementGuid>4c63134e-0087-4729-a590-55a13d51c2d9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;cart-link&quot;)/span[@class=&quot;_1fragemm9 _1fragem2i _1fragemhs _1fragemhn a8x1wua _1fragemk5 a8x1wuj a8x1wum&quot;]/svg[@class=&quot;a8x1wuo _1fragemk5 _1fragemm9 _1fragemhs _1fragemhn&quot;]</value>
      <webElementGuid>5a784555-8c29-4553-a9f7-e12f43fdc8c4</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Checkout'])[1]/following::*[name()='svg'][1]</value>
      <webElementGuid>cd34b2c1-091a-470d-8eea-7d026f45a90a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Brodo'])[1]/following::*[name()='svg'][1]</value>
      <webElementGuid>4ac118a7-8b78-4758-8598-b13fdf9678d9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Show order summary'])[1]/preceding::*[name()='svg'][1]</value>
      <webElementGuid>e9ecd68a-1149-4048-b6b2-ff7f3a4ad234</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Account'])[1]/preceding::*[name()='svg'][2]</value>
      <webElementGuid>42cce28d-7613-494d-bb1e-9db1595ca400</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
