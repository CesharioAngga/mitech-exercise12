<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Add to cart</name>
   <tag></tag>
   <elementGuidId>bb45cb31-d777-46a2-95d6-76a04dc209c7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button[name=&quot;add&quot;]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@name='add']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>6ca48b36-5be1-459c-bcfe-2e185ab4f8f6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>38b8d676-8693-4bdc-991e-3ac5116a1f55</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>add</value>
      <webElementGuid>efce420b-ce08-4c9a-9343-c9a4d998d47e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn--full add-to-cart btn--tertiary</value>
      <webElementGuid>ccde89fd-7d1a-4267-a760-bddf346c9212</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
      
        Add to cart
      
    </value>
      <webElementGuid>fd3cb9ed-04e3-48d0-b63b-a3bcb092978f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;AddToCartForm-7659509678271-modal&quot;)/div[@class=&quot;payment-buttons&quot;]/button[@class=&quot;btn btn--full add-to-cart btn--tertiary&quot;]</value>
      <webElementGuid>1ab920ef-b23b-440f-92ba-cefa838a45aa</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@name='add']</value>
      <webElementGuid>1da83348-061f-40c9-ac59-7caa1d9910fb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='AddToCartForm-7659509678271-modal']/div/button</value>
      <webElementGuid>e15727de-d1b5-41e0-b88b-cb7477db3704</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='F'])[1]/following::button[1]</value>
      <webElementGuid>4923b927-9aed-4551-82dd-e465e5430d92</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Style'])[2]/following::button[1]</value>
      <webElementGuid>ed3828ab-b3e4-4712-bca9-cdf14d0e8ec7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Buy it now'])[1]/preceding::button[1]</value>
      <webElementGuid>85d83039-5bac-4481-b49f-32f8c711023b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/form/div/button</value>
      <webElementGuid>4ff2cf72-cbe5-4631-a1ea-d2aa39d47ef1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'submit' and @name = 'add' and (text() = '
      
        Add to cart
      
    ' or . = '
      
        Add to cart
      
    ')]</value>
      <webElementGuid>8e69c315-4c9b-41c5-abc1-40dc53c98938</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
